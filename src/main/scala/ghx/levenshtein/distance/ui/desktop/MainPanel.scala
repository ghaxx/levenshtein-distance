package ghx.levenshtein.distance.ui.desktop

import javafx.event.EventHandler
import javafx.scene.input.KeyEvent

import ghx.levenshtein.distance.algorithm.LevenshteinDistanceAlgorithm

import scalafx.beans.property.StringProperty
import scalafx.geometry.Insets
import scalafx.scene.control.{Label, TextField}
import scalafx.scene.layout.{GridPane, HBox, VBox}

class MainPanel extends VBox {
  private val value = new StringProperty("")
  private val word1 = new StringProperty("")
  private val word2 = new StringProperty("")

  children.addAll(inputPanel, resultPanel)
  spacing = 5
  margin = Insets(7)

  private def calculate() = {
    value.set(LevenshteinDistanceAlgorithm(word1.value, word2.value).toString)
  }

  lazy val resultPanel = {
    val box = new HBox()
    val label = new Label("Result: ")
    box.children.addAll(label, resultLabel)
    box
  }

  lazy val resultLabel = {
    val label = new Label()
    label.textProperty().bind(value)
    label
  }

  lazy val inputPanel = {
    val box = new GridPane
    box.hgap = 5
    box.vgap = 5
    val label1 = new Label("Word 1:")
    val label2 = new Label("Word 2:")
    val word1Field = new TextField()
    val word2Field = new TextField()
    word1Field.textProperty().bindBidirectional(word1)
    word2Field.textProperty().bindBidirectional(word2)
    word1Field.onKeyReleased = handler
    word2Field.onKeyReleased = handler
    box.children.addAll(word1Field, word2Field, label1, label2)
    GridPane.setConstraints(label1, 0, 0)
    GridPane.setConstraints(label2, 1, 0)
    GridPane.setConstraints(word1Field, 0, 1)
    GridPane.setConstraints(word2Field, 1, 1)
    box
  }

  private lazy val handler = new EventHandler[KeyEvent]() {
    def handle(event: KeyEvent): Unit = {
      calculate()
    }
  }
}
