package ghx.levenshtein.distance.ui.desktop

import javafx.application.Application
import javafx.scene.Scene
import javafx.scene.layout.StackPane
import javafx.stage.Stage

import ghx.levenshtein.distance.logging.Logging

class App extends Application with Logging {
  logger.info("Starting app")

  override def start(primaryStage: Stage) {
    primaryStage.setTitle("Levenshtein Distance")

    val root = new StackPane
    val view = new MainPanel
    root.getChildren.add(view)

    primaryStage.setScene(new Scene(root))
    primaryStage.show()
  }
}

object App {
  def main(args: Array[String]) {
    Application.launch(classOf[App], args: _*)
  }
}