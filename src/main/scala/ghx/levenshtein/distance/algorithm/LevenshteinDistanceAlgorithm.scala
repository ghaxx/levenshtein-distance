package ghx.levenshtein.distance.algorithm

object LevenshteinDistanceAlgorithm {
  def apply(word1: String, word2: String): Int = {
    val d = Array.fill[Int](word1.length + 1, word2.length + 1)(0)

    for (i <- 0 to word1.length) {
      d(i)(0) = i
    }
    for (j <- 1 to word2.length) {
      d(0)(j) = j
    }

    for {
      i <- 1 to word1.length
      j <- 1 to word2.length
    } {
      val cost = if (word1(i - 1) == word2(j - 1)) 0 else 1
      val removal = d(i - 1)(j) + 1
      val insertion = d(i)(j - 1) + 1
      val change = d(i - 1)(j - 1) + cost
      d(i)(j) = min(removal, insertion, change)
    }

    d(word1.length)(word2.length)
  }

  private def min(elements: Int*): Int = elements.min

}
